# Zadanie 2

Napisz service, który po wywołaniu obróci łazika o zadany kąt

Dodatni kąt == obrót w lewo  

ujemny == obrót w prawo

### Wprowadzenie

Aby móc wiedzieć jaką drogę przebył łazik oraz jak jest zorientowany potrzebna jest jego lokalizacja. Sensorami używanymi przez Kalmana są GPS i kamery odometrii wizualnej do szacowania pozycji (x, y) oraz IMU do szacowania orientacji (kąta).
W Gazebo używamy wtyczek do symulowania powyższych sensorów.

Estymata lokalizacji bazująca nas tych sensorach publikowana jest na pojedyńczym topicu (dla zainteresowanych - [EKF w ROS](http://docs.ros.org/en/noetic/api/robot_localization/html/index.html))

W tym zadaniu interesować nas będzie jedynie orientacja łazika, z której możemy uzyskać informacje o jaki kąt obrócił się łazik.

W ROSie orientacja podawana jest w kwaternionach. Aby łatwiej sprawdzać przejechany dystans można przejść na kąty RPY. Nas interesować będzie kąt yaw - obrót względem osi Z.

[Wizualizacja kątów RPY](https://automaticaddison.com/wp-content/uploads/2020/03/yaw_pitch_rollJPG.jpg)



### 1. Uruchom symulację

```bash
roslaunch kalman_workshops_main kalman_sim_zadanie_2.launch
```
Komenda ta uruchamia plik o rozszerzeniu `.launch` z paczki `kalman_workshops_main` przygotowanej pod warsztaty


### 2. Sprawdź informacje o estymowanej lokalizacji
Estymata obecnej lokalizacji łazika względem punktu startu publikowana jest na topic `/odometry/filtered`


**Sprawdź informacje o tym topicku**

```bash
rostopic info /odometry/filtered
```
Typ tej wiadomości to `nav_msgs/Odometry` - [definicja](http://docs.ros.org/en/noetic/api/nav_msgs/html/msg/Odometry.html)

Zasubskrybuj topic z terminala

```bash
rostopic echo /odometry/filtered
```

### 3. Wykonaj zadanie
Aby obrócić się w miejscu, wystarczy publikować odpowiednią komendę prędkości na topic `/kalman/cmd_vel`

Obecną lokalizację łazika można otrzymać z topicu `/odometry/filtered`

Aby zaimplementować service możesz skorzystać ze zdefiniowanego przez nas typu `SimpleRotation.srv`. Odpowiednie moduły zaimportowane są już w pliku **.py**, w którym trzeba wykonać zadanie.

Możesz sprawdzić pola servicu wpisując w terminal polecenie

```
rossrv show kalman_workshops_utils/SimpleRotation
```


**Otwórz plik `src/rotation_server.py`**

W celu łatwiejszej realizacji zadania zaimplementowano funkcję, `yaw_from_odom`, która z wiadomości o typie `geometry_msgs/Odometry` wyciąga kąt obrotu łazika w stopniach po osi Z względem początku układu współrzędnych. 

Kąt ten przyjmuje wartości z zakresu <-180, 180> stopni

Wskazówka: 
Przy liczeniu dystansu między dwoma kolejnymi orientacjami lazika mogą wystąpić sytuacje, w których kąt będzie równy **360 - delta**. W dużym uproszczeniu wynika to z faktu, że występują krańce przedziału kąta oraz że kąt można podawać na dwa sposoby.
Funkcja `pick_shorter_distance` zawsze zwróci krótszy dystans między dwoma orientacjami

![img](img/katy.png)

Przydatny tutorial - http://wiki.ros.org/ROS/Tutorials/WritingServiceClient%28python%29

http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28python%29


### 4. Uruchom node
Uruchom symulację jak w punkcie 1, a następnie uruchamiaj node poniżsą komendą

```bash
rosrun zadanie_2 rotation_server.py
```