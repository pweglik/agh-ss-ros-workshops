# Zadanie 4

Stwórz plik launch, w którym uruchomisz node do wykrywania aruco tagów.

Do wykrywania aruco markerów możemy wykorzystac gotową paczkę `aruco_ros`, która wykrywa marker i publikuje jego polozenie na odpowiedni topic, a takze wizualizuje jego obecnosc w rviz 


1. W katalogu launch utwórz plik `zadanie_4.launch`


2. Umieść w nim poniższy kod:
```xml
<?xml version="1.0"?>
<launch>

    <node pkg="aruco_ros" type="single" name="aruco_single">
        <remap from="/camera_info" to="/d435/color/camera_info" />
        <remap from="/image" to="/d435/color/image_raw" />
        <param name="image_is_rectified" value="True"/>
        <param name="marker_size"        value="0.1778"/>
        <param name="marker_id"          value="1"/>
        <param name="reference_frame"    value="base_link"/> 
        <param name="camera_frame"       value="d435_color_optical_frame"/>
        <param name="marker_frame"       value="aruco_marker_frame" />
        <param name="corner_refinement"  value="LINES" />
    </node>

</launch>
```
`remap` w powyższym pliku zmienia nazwę topicku wewnątrz node'a.
Np - node `aruco_ros` posluguje się topickiem `/image` do subskrypcji obrazu, ale my zmieniamy go na `/d435/color/image_raw`, bo tak nazywa sie topic na ktory wypluwane sa dane z jednej z kamer

`param` definiuje wartosci parametrów jakie przyjmuje dany node

**Opis kilku nazw z powyszego pliku**
* `/camera_info` - na tym topicu `aruco_ros` subskrybuje metadane na temat kamery, min macierz kamery, ktora potrzebna jest do szacowania odleglosci taga ze zdjecia
* `/image` - na tym topicu `aruco_ros` subskrybuje obrazki z kamery

* `marker_id` - identyfikator taga. tag zespawnowany w symulacji ma id 1
* `reference_frame` - uklad odniesienia w jakim ma byc szacowane polozenie taga - w naszym przypadku jest to glowny uklad lazika znajdujacy sie w jego centrum, ktory nazywa sie `base_link`
* `camera_frame` - uklad odniesienia soczewki kamery
* `marker_frame` - nazwa ukladu odniesienia jaki nadac ma wykrytemu tagowi paczka `aruco_ros` 

Kod ten uruchomi paczkę do wykrywania tagów. Zerknij jakie argumenty przyjmuje ten node


**Kod ten uruchomimy za chwile, ale najpierw uruchomijmy symulacje lazika i zapoznajmy sie z wizualizacja**

3. Uruchom symulacje lazika
```bash
roslaunch kalman_workshops_main kalman_sim_zadanie_4.launch
```

Zauwaz, ze tym razem z lazikiem spawnowany jest nowy obiekt - marker

4. Wyswietl lazika dodając obiekt RobotModel w Rvizie

5. Wyswietl wszystkie uklady odniesienia dodając obiekt TF w Rvizie

6. Odpal utworzony przed chwila plik launch

```bash
roslaunch zadanie_4 zadanie_4.launch
```

Komenda ta wlaczy node'a `aruco_ros`, ktory pozwoli wykrywac taga 


7. Sprawdz czy w RViz pojawil sie układ odniesienia markera - `aruco_marker_frame`


8. Napisz subscribera, który będzie printował położenie markera względem łazika 
dane publikowane sa na topicu `/aruco/position`

Przyda się to do kolejnego zadania




