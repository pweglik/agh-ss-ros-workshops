# Zadanie 3

Napisz prosty path follower, który poruszać się będzie po kwadracie o boku 3m. 


### 0. Wprowadzenie
Do wykonania zadania użyjemy poniższych topiców i serviców

#### Topicki
* **/kalman/cmd_vel** - na ten topic bedziemy publikowac komendy predkosci

* **/odometry/filtered** - na tym topicu zasubksrybowac mozna informacje o obecnej lokalizacji lazika

#### Servicy

* Mozemy wykorzystac zaimplementowany w poprzednim zadaniu service


