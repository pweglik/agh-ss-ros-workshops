## Zadanie 5

Utwórz nową paczkę, w której wykryjesz marker i dojedziesz na odleglość 0.5 m od markera

0. Wprowadzenie

Do tej pory korzystaliscie ze stworzonych przez Nas folderow. Na tych folderach mogliscie wykonywac polecenia takie jak `roscd`, `rosrun`, `roslaunch` etc.

Aby moc tak robic na stworzonym przez was folderze trzeba utworzyc paczke odpowiednia komenda - `catkin_create_pkg`. catkin to build system uzywany w ROSie do budowania paczek i node'ow napisanych w cpp.

1. Przejdz do katalogu `workshops`

Po wpisaniu komendy `ls` powinny ci sie wyswietlic znane Ci paczki (zadanie_1, zadanie_2, zadanie_3 etc.)


2. Utworz paczke

`catkin_create_pkg rozwiazanie_zad_5`

3. Zbuduj paczki

`catkin build`

Ta komenda tworzy odpowiednie pliki wykonalne i foldery, ktore buduja pliki cpp znajdujace sie w paczce oraz pozwalaja korzystac z komend `roscd`, `rosrun` etc

4. Przeładuj konfiguracje rosa

`source ~/.bashrc`

Bez wchodzenia w szczegoly, ta komenda musi byc wykonana kiedy zostaje dodana nowa paczka

5. Sprawdz czy mozesz dostac sie do utworzonego folderu

`roscd rozwiazanie_zad_5`

6. Utworz katalogi `launch` i `src`

7. Wykorzystujac komponenty z poprzednich warsztatow wykonaj zadanie


Mozesz albo kopiowac kod, albo wykorzystywac istniejace juz fragmenty odpalajac je razem w launch filu - przykladowo mozesz w jednym launchfilu dodac node do wykrywania tagow i node z drugiego zadania aby moc obrocic sie na poczatku w miejscu o 180 stopni




