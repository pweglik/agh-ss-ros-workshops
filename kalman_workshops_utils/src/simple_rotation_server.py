#!/usr/bin/env python3
import rospy 

from kalman_workshops_utils.srv import SimpleRotation, SimpleRotationResponse

import actionlib    
from kalman_rover_uart.msg import RotateInPlaceAction, RotateInPlaceGoal
def handle_rotate_in_place(req):
    client = actionlib.SimpleActionClient('/rotate_in_place', RotateInPlaceAction)
    client.wait_for_server()
    client.send_goal(RotateInPlaceGoal(angle=req.degrees))
    client.wait_for_result()
    return SimpleRotationResponse(True)

if __name__ == "__main__":
    rospy.init_node('add_two_ints_server')
    s = rospy.Service('/rotate_in_place', SimpleRotation, handle_rotate_in_place)
    rospy.spin()
    