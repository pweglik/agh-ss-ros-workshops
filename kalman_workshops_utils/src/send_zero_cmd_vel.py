#!/usr/bin/env python3
import rospy 
from geometry_msgs.msg import Twist

if __name__ == "__main__":
    rospy.init_node('zero_cmd_vel')
    message_pub = rospy.Publisher("/dummy/zero_vel", Twist, queue_size=10)
    
    
    while not rospy.is_shutdown():
        message_pub.publish(Twist())
        rospy.sleep(0.1)
