#!/usr/bin/env python3

import rospy
from std_msgs.msg import String
# http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28python%29

if __name__ == '__main__':
    rospy.init_node("my_own_node")
    
    publisher = rospy.Publisher("/hello/world", String, queue_size=10)
    while True:
        msg = String()
        msg.data = "hello"
        publisher.publish(msg)
        rospy.sleep(1.0)



    