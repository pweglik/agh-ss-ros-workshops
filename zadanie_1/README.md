# Zadanie 1

### 1. Otwórz plik `wprowadzenie.py`
W pliku tym znajduje się napisany node, który co sekundę publikuje wiadomość o treści **"hello"** na topic **/hello/world**. Typ wiadomości to string


### 2. Otwórz nowy terminal i uruchom master node

```bash
roscore
```

### 3. Uruchom node `wprowadzenie.py` wywołując poniższą komendę w nowym terminalu

```bash
rosrun zadanie_1 wprowadzenie.py
```

Komenda ta uruchamia plik `wprowadzenie.py` znajdujący się w katalogu `src` w paczce `zadanie_1`

Zwróc uwagę jak napisany jest node `wprowadzenie.py`
Przyda się to w kolejnych zadaniach.

### 4. Sprawdż czy pojawił się nowy node wywołując poniższą komendę w nowym terminalu

```bash
rosnode list
```

### 5. Sprawdż wysyłaną wiadomość wpisując

```bash
rostopic echo /hello/world
```

### 6. zamknij terminal z master nodem

### 7. Uruchom symulację Kalmana
```bash
roslaunch kalman_workshops_main kalman_sim_zadanie_1.launch
```

### 8. W katalogu `src` tej paczki (`zadanie_1`) utwórz plik `drive.py`

### 9. Napisz node w pliku `drive.py`, który będzie co 0.1 sekundy wysyłał komendę prędkości do Kalmana 

Topic na który trzeba wysłać wiadomość to **/kalman/cmd_vel**

Możesz sprawdzić typ wiadomości, którą należy wysłać wpisując
```bash
rostopic info /kalman/cmd_vel
```

Po sprawdzeniu typu wiadomości możesz sprawdzić jej ciało wpisując
```bash
rosmsg show TYP_WIADOMOSCI
```

**Wskazówka**:
typy wiadomości w Pythonie importuje się w sposób jak na poniższych przykładach:

#### **geometry_msgs/Twist**

```python3
from geometry_msgs.msg import Twist
```

#### **std_msgs/Int32**
```python3
from std_msgs.msg import Int32
```

### 10. Nadaj odpowienie uprawnienia do pliku
`chmod a+x drive.py`

### 11. Uruchom węzeł i sprawdź czy łazik porusza się zgodnie z oczekiwaniami

### 12. Poeksperymentuj z komendami prędkości 
Wiedza ta przyda się w kolejnych zadaniach :) 

### Przydatne linki
http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28python%29