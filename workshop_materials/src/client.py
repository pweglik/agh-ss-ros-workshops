#!/usr/bin/env python3

from std_srvs.srv import SetBool, SetBoolRequest
import rospy


if __name__ == "__main__":
    rospy.init_node('set_bool_client')
    service_client = rospy.ServiceProxy('set_bool_server', SetBool)
    
    target_value = True

    req = SetBoolRequest(data=target_value)
    response = service_client(req)
    
    print(response)
