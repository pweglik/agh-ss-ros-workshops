#!/usr/bin/env python3
import rospy
from std_msgs.msg import String

if __name__ == '__main__':
    rospy.init_node("publisher_node")

    publisher = rospy.Publisher("/hello/world", String, queue_size=1)

    while True:
        publisher.publish("hello")
        rospy.sleep(1.0)
