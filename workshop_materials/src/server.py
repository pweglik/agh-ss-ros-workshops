#!/usr/bin/env python3

from std_srvs.srv import SetBool, SetBoolResponse
import rospy

some_bool_value = False

def handle_set_bool(req):
    print(f"Setting bool to {req.data}")
    some_bool_value = req.data
    return SetBoolResponse(
        success=True,
        message=f"current bool value is {some_bool_value}"
    )

if __name__ == "__main__":
    rospy.init_node('set_bool_server_server')
    s = rospy.Service('set_bool_server', SetBool, handle_set_bool)
    
    rospy.spin()
