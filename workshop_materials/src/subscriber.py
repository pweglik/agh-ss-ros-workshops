#!/usr/bin/env python3
import rospy
from std_msgs.msg import String

odometry = None

def hello_world_callback(msg):
    print(msg.data)

if __name__ == '__main__':
    rospy.init_node("subscriber_node")

    subscriber = rospy.Subscriber("/hello/world", String, hello_world_callback)
    rospy.spin()

